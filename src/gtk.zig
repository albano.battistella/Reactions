/// Additional helpers for GTK4.
const c = @import("c.zig");
const std = @import("std");

pub fn G_CALLBACK(ptr: anytype) (fn () callconv(.C) void) {
    return @ptrCast(fn () callconv(.C) void, ptr);
}

pub fn g_signal_connect(instance: c.gpointer, detailed_signal: [*c]const c.gchar, c_handler: c.GCallback, data: c.gpointer) c.gulong {
    var zero: u32 = 0;
    const flags: *c.GConnectFlags = @ptrCast(*c.GConnectFlags, &zero);
    return c.g_signal_connect_data(instance, detailed_signal, c_handler, data, null, flags.*);
}
