pub const app_name = "Reactions";
pub const app_about_description = "Simple GIF search.";

pub const action_preferences = "Preferences";
pub const action_about = "About Reactions";

pub const search_placeholder = "Search for an image...";
pub const search_load_more = "Load More";

pub const welcome_title = "Welcome to Reactions!";
pub const welcome_subtitle = "Type to search for an image.";

pub const details_drag_description = "Drag and drop to use this image.";
pub const details_get_link = "Get Link";
pub const details_copy = "Copy To Clipboard";

pub const notification_copied_link = "Web link copied to clipboard.";
pub const notification_copied_image = "Image copied to clipboard.";

pub const preferences_group_image = "Image Settings";
pub const preferences_preferred_format = "Preferred Image Format";

// TRANSLATORS: Two-character language code for the current language, used for the GIPHY API.
// See https://developers.giphy.com/docs/optional-settings/#language-support
pub const giphy_api_language = "en";

pub const translator_credits = "translator-credits";
