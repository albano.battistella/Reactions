const std = @import("std");
const c = @import("../c.zig");
const loc = @import("../localized.zig");
const App = @import("../app.zig").App;
const ImageFormat = @import("../image_format.zig").ImageFormat;
const ImageMetadata = @import("../image_metadata.zig").ImageMetadata;
const NetSession = @import("../net.zig").NetSession;
const NetMessageReceiver = @import("../net.zig").NetMessageReceiver;

pub const GiphyProvider = struct {
    const api_key: []const u8 = "pHKhKm5dfj14AxJTUkntGCwU0cI3Z8vv";

    receiver: NetMessageReceiver,
    allocator: *std.mem.Allocator,
    app: ?*App,

    pub fn init(allocator: *std.mem.Allocator) GiphyProvider {
        return GiphyProvider{ .receiver = NetMessageReceiver.initForBytes(allocator, receive, handleNetError), .allocator = allocator, .app = null };
    }

    pub fn search(self: *GiphyProvider, net_session: *NetSession, search_string: []const u8, search_number: i32, app: *App) void {
        std.log.info("Searching with GIPHY for {s}", .{search_string});

        self.app = app;

        var search_null_terminated = self.allocator.dupeZ(u8, search_string) catch unreachable;
        defer self.allocator.free(search_null_terminated);

        const escaped_search = c.g_uri_escape_string(search_null_terminated.ptr, null, @boolToInt(false));
        defer c.free(escaped_search);

        const escaped_sentinel: [*:0]const u8 = escaped_search;

        var url_array_list = std.ArrayList(u8).init(self.allocator);
        defer url_array_list.deinit();

        const results_per_search: i32 = 6;
        const offset = search_number * results_per_search;

        const lang = c.gettext(loc.giphy_api_language);

        // TODO Is there a clearer way to declare that this needs to be a null-terminated string?
        std.fmt.format(url_array_list.writer(), "https://api.giphy.com/v1/gifs/search?api_key={s}&q={s}&limit={}&offset={}&lang={s}\x00", .{ api_key, escaped_sentinel, results_per_search, offset, lang }) catch unreachable;

        std.log.info("{s}", .{url_array_list.items});
        net_session.send(url_array_list.items, &self.receiver, "application/json\x00");
    }

    fn receive(receiver: *NetMessageReceiver, message: []const u8) void {
        var self = @fieldParentPtr(GiphyProvider, "receiver", receiver);

        var results = std.ArrayList(ImageMetadata).init(self.allocator);
        errdefer {
            for (results.items) |*result| {
                result.deinit(self.allocator);
            }
        }

        defer results.deinit();

        var errors = std.ArrayList(u8).init(self.allocator);
        defer errors.deinit();

        const copy_strings = false;
        var parser = std.json.Parser.init(self.allocator, copy_strings);
        defer parser.deinit();

        var tree = parser.parse(message) catch |err| {
            std.log.err("Parsing GIPHY json failed with {s}.", .{@errorName(err)});
            return;
        }; // TODO proper error handling.
        defer tree.deinit();

        const maybe_gifs_list = tree.root.Object.get("data");

        // Must have an app to be receiving results.
        // TODO Is there a cleaner way to init so that we don't have an optional ptr?
        const app = self.app.?;

        const preferred_format = app.settings.getPreferredImageFormat();
        const image_formats = getImageFormatKeys(preferred_format);

        if (maybe_gifs_list) |gifs_list| {
            for (gifs_list.Array.items) |gif, idx| {
                var success = true;
                var title: []const u8 = "";
                var full_url: []const u8 = "";
                var full_format: ImageFormat = .mp4;
                var preview_url: []const u8 = "";

                if (gif.Object.get("title")) |found_title| {
                    title = found_title.String;
                } else {
                    title = "Untitled";
                }

                if (gif.Object.get("images")) |images| {
                    if (images.Object.get("original")) |original| {
                        var found = false;
                        for (image_formats) |pair| {
                            std.log.info("Looking for pair {s}", .{pair.key});
                            if (original.Object.get(pair.key)) |found_url| {
                                full_url = found_url.String;
                                full_format = pair.format;
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            errors.writer().print("Couldn't find a full image field for gif {}.\n", .{idx}) catch unreachable;
                            success = false;
                        }
                    } else {
                        errors.writer().print("Couldn't find 'original' field for gif {}.\n", .{idx}) catch unreachable;
                        success = false;
                    }

                    if (images.Object.get("fixed_width")) |fixed_width| {
                        // This image will be used for the preview.
                        // Prefer mp4, then gif, then fail. Don't care about user preference since it's internal only,
                        // prefer the smaller filesize.
                        // Webp would be preferable to gif here but can't be played with the ffmpeg backed (https://trac.ffmpeg.org/ticket/4907)
                        if (fixed_width.Object.get("mp4")) |mp4| {
                            preview_url = mp4.String;
                        } else if (fixed_width.Object.get("url")) |url| {
                            preview_url = url.String;
                        } else {
                            errors.writer().print("Couldn't find a preview url to use for gif {}.\n", .{idx}) catch unreachable;
                            success = false;
                        }
                    }
                } else {
                    errors.writer().print("Couldn't find 'images' field for gif {}.\n", .{idx}) catch unreachable;
                    success = false;
                }

                if (success) {
                    var alloc_title = self.allocator.dupeZ(u8, title) catch unreachable;
                    var alloc_preview = self.allocator.dupe(u8, preview_url) catch unreachable;
                    var alloc_full = self.allocator.dupe(u8, full_url) catch unreachable;
                    const result = ImageMetadata.init(alloc_title, alloc_preview, alloc_full, full_format);
                    results.append(result) catch unreachable;
                }
            }
        } else {
            errors.writer().print("Couldn't find 'data' field in json.", .{}) catch unreachable;
        }

        for (results.items) |result| {
            std.log.info("Result \'{s}\' {s} {s}", .{ result.title, result.full_url, result.preview_url });
        }

        app.addResults(results);

        if (errors.items.len > 0) {
            const errors_null_term = self.allocator.dupeZ(u8, errors.items) catch unreachable;
            defer self.allocator.free(errors_null_term);

            std.log.err("{s}", .{errors.items});

            app.handleError("Errors occurred receiving data from GIPHY.", errors_null_term);
        }
    }

    fn handleNetError(receiver: *NetMessageReceiver, message: [:0]const u8) void {
        var self = @fieldParentPtr(GiphyProvider, "receiver", receiver);

        const friendly_message = "Failed to contact GIPHY.";

        if (self.app) |app| {
            app.handleError(friendly_message, message);
        }
    }

    const FormatPair = struct {
        key: [:0]const u8,
        format: ImageFormat,
    };

    const prefer_mp4 = [_]FormatPair{ .{ .key = "mp4", .format = .mp4 }, .{ .key = "url", .format = .gif } };
    const prefer_gif = [_]FormatPair{ .{ .key = "url", .format = .gif }, .{ .key = "mp4", .format = .mp4 } };

    /// Returns a list of json keys to search for to find the full image URL, in descending order of preference.
    fn getImageFormatKeys(preferred_format: ImageFormat) []const FormatPair {
        return switch (preferred_format) {
            .mp4 => prefer_mp4[0..],
            .gif => prefer_gif[0..],
        };
    }
};
