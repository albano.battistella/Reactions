const std = @import("std");
const App = @import("../app.zig").App;
const GiphyProvider = @import("giphy_provider.zig").GiphyProvider;
const NetSession = @import("../net.zig").NetSession;

pub const ApiProvider = union(enum) {
    giphy: GiphyProvider,

    pub fn init(allocator: *std.mem.Allocator) ApiProvider {
        return ApiProvider{ .giphy = GiphyProvider.init(allocator) };
    }
    
    pub fn search(self: *ApiProvider, net_session: *NetSession, search_string: []const u8, search_number: i32, app: *App) void {
        switch (self.*) {
            .giphy => |*provider| provider.search(net_session, search_string, search_number, app),
        }
    }
};
