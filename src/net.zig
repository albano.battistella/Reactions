const c = @import("c.zig");
const std = @import("std");

pub const NetMessageReceiver = struct {
    receiveFn: ?fn (*NetMessageReceiver, []const u8) void,
    errorFn: ?fn (*NetMessageReceiver, [:0]const u8) void,
    received: std.ArrayList(u8),

    pub fn initForBytes(allocator: *std.mem.Allocator, receiveFn: fn (*NetMessageReceiver, []const u8) void, errorFn: fn (*NetMessageReceiver, [:0]const u8) void) NetMessageReceiver {
        return NetMessageReceiver{
            .receiveFn = receiveFn,
            .errorFn = errorFn,
            .received = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn receiveBytes(self: *NetMessageReceiver, message: []const u8) void {
        if (self.receiveFn) |fun| {
            fun(self, message);
        }
    }

    pub fn handleError(self: *NetMessageReceiver, err: [:0]const u8) void {
        std.log.info("Got an error ({s}).", .{err});

        if (self.errorFn) |fun| {
            fun(self, err);
        }
    }
};

pub const NetSession = struct {
    session: ?*c.SoupSession,

    pub fn init() NetSession {
        return NetSession{
            .session = c.soup_session_new(),
        };
    }

    pub fn deinit(self: *const NetSession) void {
        c.g_object_unref(self.session);
    }

    pub fn send(self: *NetSession, request: []const u8, response_receiver: *NetMessageReceiver, accept: ?[]const u8) void {
        std.log.info("Creating soup message...", .{});
        var message: *c.SoupMessage = c.soup_message_new("GET", @ptrCast([*c]const u8, request));

        if (accept) |accept_slice| {
            c.soup_message_headers_append(message.request_headers, "Accept", accept_slice.ptr);
        }

        std.log.info("Sending...", .{});
        c.soup_session_send_async(self.session, message, null, onSessionSendResponse, @ptrCast(c.gpointer, response_receiver));
        std.log.info("Sent.", .{});
    }

    fn onSessionSendResponse(object: [*c]c.GObject, result: ?*c.GAsyncResult, user_data: c.gpointer) callconv(.C) void {
        std.log.info("Received response", .{});

        var g_error: [*c]c.GError = null;
        const stream = c.soup_session_send_finish(@ptrCast(*c.SoupSession, object), result, &g_error);

        if (g_error) |err| {
            const receiver = @ptrCast(*NetMessageReceiver, @alignCast(8, user_data));

            const read_error: c.GError = err.*;
            const err_ptr = @ptrCast([*:0]const u8, read_error.message);

            receiver.handleError(std.mem.sliceTo(err_ptr, 0));

            return;
        }

        c.g_input_stream_read_bytes_async(stream, std.math.maxInt(u32), c.G_PRIORITY_DEFAULT, null, onStreamBytesRead, user_data);
    }

    fn onStreamBytesRead(object: [*c]c.GObject, result: ?*c.GAsyncResult, user_data: c.gpointer) callconv(.C) void {
        std.log.info("Receiving bytes...", .{});
        const stream = @ptrCast(*c.GInputStream, object);

        const bytes = c.g_input_stream_read_bytes_finish(stream, result, null);
        defer c.g_bytes_unref(bytes);

        const data_ptr = @ptrCast([*]const u8, c.g_bytes_get_data(bytes, null));
        const count = c.g_bytes_get_size(bytes);

        std.log.info("Bytes contains {} count ({*})", .{ count, data_ptr });

        const receiver = @ptrCast(*NetMessageReceiver, @alignCast(8, user_data));

        if (count == 0) {
            defer c.g_object_unref(stream);
            receiver.receiveBytes(receiver.received.items);
            receiver.received.clearAndFree();
        } else {
            const slice = data_ptr[0..count];
            receiver.received.appendSlice(slice) catch unreachable;
            c.g_input_stream_read_bytes_async(stream, std.math.maxInt(u32), c.G_PRIORITY_DEFAULT, null, onStreamBytesRead, user_data);
        }
    }
};
