const std = @import("std");
const ImageFormat = @import("image_format.zig").ImageFormat;

pub const ImageMetadata = struct {
    /// Title is null-terminated
    title: [:0]u8,
    preview_url: []u8,
    full_url: []u8,
    full_format: ImageFormat,

    pub fn init(title: [:0]u8, preview_url: []u8, full_url: []u8, full_format: ImageFormat) ImageMetadata {
        return ImageMetadata{
            .title = title,
            .preview_url = preview_url,
            .full_url = full_url,
            .full_format = full_format,
        };
    }

    /// Slices are owned.
    pub fn deinit(self: *ImageMetadata, allocator: *std.mem.Allocator) void {
        allocator.free(self.title);
        allocator.free(self.preview_url);
        allocator.free(self.full_url);
    }
};
