const App = @import("app.zig").App;
const LoadedImage = @import("app.zig").LoadedImage;
const PreferencesWindow = @import("preferences_window.zig").PreferencesWindow;
const c = @import("c.zig");
const config = @import("config.zig");
const gtk = @import("gtk.zig");
const loc = @import("localized.zig");
const std = @import("std");

pub const MainWindow = struct {
    const PrimaryView = struct {
        main_box: *c.GtkWidget,
        search_entry: *c.GtkWidget,

        welcome_page: *c.GtkWidget,

        results_flow_box: *c.GtkFlowBox,
        results_widgets: std.ArrayList(*c.GtkButton),
        results_preview_videos: std.ArrayList(*c.GtkMediaFile),

        load_more_button: *c.GtkWidget,
        attribution_widget: *c.GtkWidget,

        error_info: *c.GtkWidget,
        error_info_label: *c.GtkWidget,
    };

    const DetailsView = struct {
        title_label: *c.GtkWidget,
        main_box: *c.GtkWidget,
        back_button: *c.GtkWidget,
        full_video: ?*c.GtkMediaFile,
        video_widget: *c.GtkPicture,
        video_placeholder_spinner: *c.GtkWidget,

        link_button: *c.GtkWidget,
        copy_button: *c.GtkWidget,

        loaded_image: ?*const LoadedImage,
    };

    app: *App,
    gtk_window: *c.GtkWindow,

    root_stack: *c.GtkStack,

    primary_view: PrimaryView,
    details_view: DetailsView,
    preferences_window: ?PreferencesWindow,

    pub fn init(app: *App) MainWindow {
        const adw_app = app.adw_app;
        const window = c.adw_application_window_new(@ptrCast(*c.GtkApplication, adw_app));
        const gtk_window = @ptrCast([*c]c.GtkWindow, window);

        _ = gtk.g_signal_connect(window, "destroy", gtk.G_CALLBACK(onWindowDestroyed), @ptrCast(c.gpointer, app));

        c.gtk_window_set_title(gtk_window, c.gettext(loc.app_name));
        c.gtk_window_set_default_size(gtk_window, 500, 720);

        // The root stack switches between the primary search view, and the details view for a selected gif.
        const root_stack = @ptrCast(*c.GtkStack, c.gtk_stack_new());
        c.gtk_stack_set_transition_type(root_stack, c.GtkStackTransitionType.GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT);

        c.adw_application_window_set_content(@ptrCast(*c.AdwApplicationWindow, window), @ptrCast(*c.GtkWidget, root_stack));

        const primary_view = buildPrimaryView(app, window);
        _ = c.gtk_stack_add_child(root_stack, @ptrCast(*c.GtkWidget, primary_view.main_box));

        _ = c.gtk_widget_grab_focus(primary_view.search_entry);

        const details_view = buildDetailsView(app, window);
        _ = c.gtk_stack_add_child(root_stack, @ptrCast(*c.GtkWidget, details_view.main_box));

        c.gtk_widget_show(window);

        return MainWindow{
            .app = app,
            .gtk_window = gtk_window,
            .root_stack = root_stack,
            .primary_view = primary_view,
            .details_view = details_view,
            .preferences_window = null,
        };
    }

    fn buildPrimaryView(app: *App, window: *c.GtkWidget) PrimaryView {
        const header = c.adw_header_bar_new();

        const menu_button = c.gtk_menu_button_new();
        c.gtk_menu_button_set_icon_name(@ptrCast(*c.GtkMenuButton, menu_button), "open-menu-symbolic");

        const primary_menu = c.g_menu_new();
        c.g_menu_append(primary_menu, c.gettext(loc.action_preferences), "app.preferences");
        c.g_menu_append(primary_menu, c.gettext(loc.action_about), "app.about");

        c.gtk_menu_button_set_menu_model(@ptrCast(*c.GtkMenuButton, menu_button), @ptrCast(*c.GMenuModel, @alignCast(@alignOf(*c.GMenuModel), primary_menu)));

        const popover = c.gtk_popover_menu_new_from_model(null);

        c.adw_header_bar_pack_end(@ptrCast(*c.AdwHeaderBar, header), menu_button);

        const search_entry = c.gtk_search_entry_new();
        c.gtk_widget_set_size_request(search_entry, 200, -1);

        var g_value: c.GValue = std.mem.zeroes(c.GValue);
        _ = c.g_value_init(&g_value, c.G_TYPE_STRING);
        c.g_value_set_string(&g_value, c.gettext(loc.search_placeholder));
        c.g_object_set_property(@ptrCast(*c.GObject, search_entry), "placeholder-text", &g_value);

        c.adw_header_bar_set_title_widget(@ptrCast(*c.AdwHeaderBar, header), search_entry);

        const scrollable = c.gtk_scrolled_window_new();
        c.gtk_widget_set_vexpand(scrollable, @boolToInt(true));

        const results_flow_box = c.gtk_flow_box_new();

        const load_more_button = c.gtk_button_new_with_label(c.gettext(loc.search_load_more));
        c.gtk_widget_hide(load_more_button);

        const welcome_page = c.adw_status_page_new();
        c.adw_status_page_set_title(@ptrCast(*c.AdwStatusPage, welcome_page), c.gettext(loc.welcome_title));
        c.adw_status_page_set_description(@ptrCast(*c.AdwStatusPage, welcome_page), c.gettext(loc.welcome_subtitle));
        c.adw_status_page_set_icon_name(@ptrCast(*c.AdwStatusPage, welcome_page), config.app_id);
        c.gtk_widget_set_vexpand(welcome_page, @boolToInt(true));
        c.gtk_widget_set_margin_end(welcome_page, 10);

        var attrib_picture_path = std.ArrayList(u8).init(app.allocator);
        defer attrib_picture_path.deinit();
        std.fmt.format(attrib_picture_path.writer(), "{s}/giphy.png\x00", .{config.data_dir}) catch unreachable;

        const attrib_picture = c.gtk_picture_new_for_filename(attrib_picture_path.items.ptr);
        c.gtk_widget_set_halign(attrib_picture, c.GtkAlign.GTK_ALIGN_CENTER);
        c.gtk_widget_set_valign(attrib_picture, c.GtkAlign.GTK_ALIGN_CENTER);
        c.gtk_widget_set_size_request(attrib_picture, -1, 20);
        c.gtk_widget_hide(attrib_picture);

        const child_box = c.gtk_box_new(c.GtkOrientation.GTK_ORIENTATION_VERTICAL, 10);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), attrib_picture);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), results_flow_box);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), load_more_button);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), welcome_page);

        c.gtk_widget_set_margin_top(child_box, 10);
        c.gtk_widget_set_margin_bottom(child_box, 10);
        c.gtk_widget_set_margin_start(child_box, 10);
        c.gtk_widget_set_margin_end(child_box, 10);

        const clamp = c.adw_clamp_new();
        c.adw_clamp_set_child(@ptrCast(*c.AdwClamp, clamp), child_box);

        c.gtk_scrolled_window_set_child(@ptrCast(*c.GtkScrolledWindow, scrollable), clamp);

        const error_info = c.gtk_info_bar_new();
        const error_label = c.gtk_label_new("");
        c.gtk_label_set_wrap(@ptrCast(*c.GtkLabel, error_label), @boolToInt(true));
        c.gtk_info_bar_add_child(@ptrCast(*c.GtkInfoBar, error_info), error_label);
        c.gtk_info_bar_set_revealed(@ptrCast(*c.GtkInfoBar, error_info), @boolToInt(false));
        c.gtk_info_bar_set_message_type(@ptrCast(*c.GtkInfoBar, error_info), c.GtkMessageType.GTK_MESSAGE_WARNING);
        c.gtk_info_bar_set_show_close_button(@ptrCast(*c.GtkInfoBar, error_info), @boolToInt(true));
        _ = gtk.g_signal_connect(@ptrCast(*c.GtkInfoBar, error_info), "response", gtk.G_CALLBACK(onErrorBarClicked), null);

        const main_box = c.gtk_box_new(c.GtkOrientation.GTK_ORIENTATION_VERTICAL, 0);
        c.gtk_box_append(@ptrCast(*c.GtkBox, main_box), header);
        c.gtk_box_append(@ptrCast(*c.GtkBox, main_box), error_info);
        c.gtk_box_append(@ptrCast(*c.GtkBox, main_box), @ptrCast(*c.GtkWidget, scrollable));

        return PrimaryView{
            .main_box = main_box,
            .search_entry = search_entry,
            .welcome_page = welcome_page,
            .results_flow_box = @ptrCast(*c.GtkFlowBox, results_flow_box),
            .results_widgets = std.ArrayList(*c.GtkButton).init(app.allocator),
            .results_preview_videos = std.ArrayList(*c.GtkMediaFile).init(app.allocator),
            .load_more_button = load_more_button,
            .attribution_widget = attrib_picture,
            .error_info = error_info,
            .error_info_label = error_label,
        };
    }

    fn buildDetailsView(app: *App, window: *c.GtkWidget) DetailsView {
        const header = c.adw_header_bar_new();

        const title_label = c.gtk_label_new(null);
        c.gtk_label_set_ellipsize(@ptrCast(*c.GtkLabel, title_label), c.PangoEllipsizeMode.PANGO_ELLIPSIZE_END);

        const title_style = c.gtk_widget_get_style_context(title_label);
        c.gtk_style_context_add_class(title_style, "title");

        c.adw_header_bar_set_title_widget(@ptrCast(*c.AdwHeaderBar, header), title_label);

        const back_button = c.gtk_button_new_from_icon_name("go-previous-symbolic");
        c.adw_header_bar_pack_start(@ptrCast(*c.AdwHeaderBar, header), @ptrCast(*c.GtkWidget, back_button));

        const video_placeholder_spinner = c.gtk_spinner_new();

        const video_widget = c.gtk_picture_new();

        const frame = c.gtk_frame_new(null);
        c.gtk_frame_set_child(@ptrCast(*c.GtkFrame, frame), video_widget);

        const label = c.gtk_label_new(c.gettext(loc.details_drag_description)); // TODO Caption style.

        const link_button = c.gtk_button_new_with_label(c.gettext(loc.details_get_link));

        const copy_button = c.gtk_button_new_with_label(c.gettext(loc.details_copy));

        const buttons_row = c.gtk_box_new(c.GtkOrientation.GTK_ORIENTATION_HORIZONTAL, 0);
        c.gtk_box_append(@ptrCast(*c.GtkBox, buttons_row), link_button);
        c.gtk_box_append(@ptrCast(*c.GtkBox, buttons_row), copy_button);
        c.gtk_widget_set_halign(buttons_row, c.GtkAlign.GTK_ALIGN_CENTER);

        const style = c.gtk_widget_get_style_context(buttons_row);
        c.gtk_style_context_add_class(style, "linked");

        const child_box = c.gtk_box_new(c.GtkOrientation.GTK_ORIENTATION_VERTICAL, 10);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), video_placeholder_spinner);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), frame);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), label);
        c.gtk_box_append(@ptrCast(*c.GtkBox, child_box), buttons_row);

        c.gtk_widget_set_margin_top(child_box, 10);
        c.gtk_widget_set_margin_bottom(child_box, 10);
        c.gtk_widget_set_margin_start(child_box, 10);
        c.gtk_widget_set_margin_end(child_box, 10);

        const clamp = c.adw_clamp_new();
        c.adw_clamp_set_child(@ptrCast(*c.AdwClamp, clamp), child_box);

        const main_box = c.gtk_box_new(c.GtkOrientation.GTK_ORIENTATION_VERTICAL, 10);
        c.gtk_box_append(@ptrCast(*c.GtkBox, main_box), header);
        c.gtk_box_append(@ptrCast(*c.GtkBox, main_box), clamp);

        return DetailsView{
            .title_label = title_label,
            .main_box = main_box,
            .back_button = back_button,
            .full_video = null,
            .video_widget = @ptrCast(*c.GtkPicture, video_widget),
            .video_placeholder_spinner = video_placeholder_spinner,
            .link_button = link_button,
            .copy_button = copy_button,

            .loaded_image = null,
        };
    }

    pub fn deinit(self: *MainWindow) void {
        self.primary_view.results_preview_videos.clearAndFree();
        self.primary_view.results_widgets.clearAndFree();
    }

    /// Connect the signals used by this window. Signals will require a pointer to the struct, so cannot
    /// be connected during init.
    pub fn connectSignals(self: *MainWindow) void {
        _ = gtk.g_signal_connect(self.primary_view.search_entry, "activate", gtk.G_CALLBACK(onSearchActivated), @ptrCast(c.gpointer, self));
        _ = gtk.g_signal_connect(self.details_view.back_button, "clicked", gtk.G_CALLBACK(onBackToPrimaryClicked), @ptrCast(c.gpointer, self));
        _ = gtk.g_signal_connect(self.primary_view.load_more_button, "clicked", gtk.G_CALLBACK(onLoadMoreClicked), @ptrCast(c.gpointer, self));

        _ = gtk.g_signal_connect(self.details_view.link_button, "clicked", gtk.G_CALLBACK(onCopyWebLinkClicked), @ptrCast(c.gpointer, self));
        _ = gtk.g_signal_connect(self.details_view.copy_button, "clicked", gtk.G_CALLBACK(onCopyImageClicked), @ptrCast(c.gpointer, self));

        const preferences_action = c.g_simple_action_new("preferences", null);
        c.g_action_map_add_action(@ptrCast(*c.GActionMap, self.app.adw_app), @ptrCast(*c.GAction, preferences_action));
        _ = gtk.g_signal_connect(preferences_action, "activate", gtk.G_CALLBACK(onPreferencesClicked), @ptrCast(c.gpointer, self));

        const about_action = c.g_simple_action_new("about", null);
        c.g_action_map_add_action(@ptrCast(*c.GActionMap, self.app.adw_app), @ptrCast(*c.GAction, about_action));
        _ = gtk.g_signal_connect(about_action, "activate", gtk.G_CALLBACK(onAboutClicked), @ptrCast(c.gpointer, self));
    }

    pub fn addResult(self: *MainWindow, image: *LoadedImage) void {
        var result_button = @ptrCast(*c.GtkButton, c.gtk_button_new());
        c.gtk_widget_set_size_request(@ptrCast(*c.GtkWidget, result_button), -1, 200);

        c.gtk_flow_box_insert(self.primary_view.results_flow_box, @ptrCast([*c]c.GtkWidget, result_button), -1);

        std.log.info("Loading button contents.", .{});
        var mediafile = c.gtk_media_file_new_for_input_stream(image.preview_image_stream);
        var video_widget = c.gtk_picture_new_for_paintable(@ptrCast(*c.GdkPaintable, mediafile));
        c.gtk_media_stream_set_loop(@ptrCast(*c.GtkMediaStream, mediafile), @boolToInt(true));
        c.gtk_media_stream_play(@ptrCast(*c.GtkMediaStream, mediafile));

        c.gtk_button_set_child(result_button, @ptrCast([*c]c.GtkWidget, video_widget));
        c.gtk_button_set_has_frame(result_button, @boolToInt(false));

        _ = gtk.g_signal_connect(result_button, "clicked", gtk.G_CALLBACK(onResultClicked), @ptrCast(c.gpointer, image));

        self.primary_view.results_widgets.append(result_button) catch unreachable;
        self.primary_view.results_preview_videos.append(@ptrCast(*c.GtkMediaFile, mediafile)) catch unreachable;

        c.gtk_widget_show(self.primary_view.load_more_button);
        c.gtk_widget_hide(self.primary_view.welcome_page);
        c.gtk_widget_show(self.primary_view.attribution_widget);
    }

    pub fn clearResults(self: *MainWindow) void {
        self.clearDetailsView();

        for (self.primary_view.results_preview_videos.items) |mediafile| {
            c.gtk_media_file_clear(mediafile);
            c.g_object_unref(mediafile);
        }

        self.primary_view.results_preview_videos.clearAndFree();

        for (self.primary_view.results_widgets.items) |widget| {
            var parent = c.gtk_widget_get_parent(@ptrCast(*c.GtkWidget, widget));

            c.gtk_flow_box_remove(self.primary_view.results_flow_box, parent);
        }

        self.primary_view.results_widgets.clearAndFree();

        c.gtk_widget_hide(self.primary_view.load_more_button);
        c.gtk_widget_hide(self.primary_view.attribution_widget);
    }

    pub fn viewDetails(self: *MainWindow, image: *LoadedImage) void {
        var selection_model = c.gtk_stack_get_pages(self.root_stack);
        _ = c.gtk_selection_model_select_item(selection_model, 1, @boolToInt(true));

        c.gtk_label_set_label(@ptrCast(*c.GtkLabel, self.details_view.title_label), image.image.title);

        const video_container = c.gtk_widget_get_parent(@ptrCast(*c.GtkWidget, @alignCast(@alignOf(*c.GtkWidget), self.details_view.video_widget)));
        c.gtk_widget_hide(video_container);

        c.gtk_widget_show(self.details_view.video_placeholder_spinner);
        c.gtk_spinner_start(@ptrCast(*c.GtkSpinner, self.details_view.video_placeholder_spinner));
    }

    pub fn showImage(self: *MainWindow, image: *const LoadedImage) void {
        self.clearDetailsView();

        self.details_view.full_video = @ptrCast(*c.GtkMediaFile, c.gtk_media_file_new_for_input_stream(image.full_image_stream));
        c.gtk_picture_set_paintable(self.details_view.video_widget, @ptrCast(*c.GdkPaintable, self.details_view.full_video));

        c.gtk_media_stream_set_loop(@ptrCast(*c.GtkMediaStream, self.details_view.full_video), @boolToInt(true));
        c.gtk_media_stream_play(@ptrCast(*c.GtkMediaStream, self.details_view.full_video));

        const video_container = c.gtk_widget_get_parent(@ptrCast(*c.GtkWidget, @alignCast(@alignOf(*c.GtkWidget), self.details_view.video_widget)));
        c.gtk_widget_show(video_container);

        c.gtk_widget_hide(self.details_view.video_placeholder_spinner);
        c.gtk_spinner_stop(@ptrCast(*c.GtkSpinner, self.details_view.video_placeholder_spinner));

        self.details_view.loaded_image = image;
    }

    pub fn setDragAndDrop(self: *MainWindow, provider: *c.GdkContentProvider) void {
        std.log.info("Adding drag and drop.", .{});
        var drag_source = c.gtk_drag_source_new();
        _ = gtk.g_signal_connect(drag_source, "drag-begin", gtk.G_CALLBACK(onDragBegin), self.gtk_window);

        c.gtk_drag_source_set_content(drag_source, provider);
        c.gtk_drag_source_set_actions(drag_source, c.GdkDragAction.GDK_ACTION_COPY);

        c.gtk_widget_add_controller(@ptrCast(*c.GtkWidget, @alignCast(@alignOf(*c.GtkWidget), self.details_view.video_widget)), @ptrCast(*c.GtkEventController, drag_source));
    }

    pub fn showError(self: *MainWindow, friendly_message: [:0]const u8, details: [:0]const u8) void {
        var error_message_array_list = std.ArrayList(u8).init(self.app.allocator);
        defer error_message_array_list.deinit();

        std.fmt.format(error_message_array_list.writer(), "<span weight='bold'>{s}</span>\n({s})\x00", .{ friendly_message, details }) catch unreachable;
        std.log.err("{s}", .{error_message_array_list.items});

        c.gtk_info_bar_set_revealed(@ptrCast(*c.GtkInfoBar, self.primary_view.error_info), @boolToInt(true));
        c.gtk_label_set_markup(@ptrCast(*c.GtkLabel, self.primary_view.error_info_label), error_message_array_list.items.ptr);

        self.backToPrimaryView();
    }

    fn clearDetailsView(self: *MainWindow) void {
        c.gtk_picture_set_paintable(self.details_view.video_widget, null);

        if (self.details_view.full_video) |mediafile| {
            c.gtk_media_file_clear(mediafile);
            c.g_object_unref(mediafile);
        }

        self.details_view.full_video = null;
        self.details_view.loaded_image = null;
    }

    fn backToPrimaryView(self: *MainWindow) void {
        var selection_model = c.gtk_stack_get_pages(self.root_stack);
        _ = c.gtk_selection_model_select_item(selection_model, 0, @boolToInt(true));
    }

    fn onSearchActivated(search_entry: *c.GtkSearchEntry, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        const search_text: [*:0]const u8 = @ptrCast([*:0]const u8, c.gtk_editable_get_text(@ptrCast(*c.GtkEditable, search_entry)));
        std.log.info("Searched for {s}", .{search_text});
        self.app.search(std.mem.sliceTo(search_text, 0));
    }

    fn onResultClicked(button: *c.GtkButton, user_data: c.gpointer) callconv(.C) void {
        var image = @ptrCast(*LoadedImage, @alignCast(@alignOf(*LoadedImage), user_data));

        image.requestFull();
    }

    fn onLoadMoreClicked(button: *c.GtkButton, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        self.app.loadMoreResults();
    }

    fn onBackToPrimaryClicked(button: *c.GtkButton, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));
        self.backToPrimaryView();
    }

    fn onCopyWebLinkClicked(button: *c.GtkButton, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        if (self.details_view.loaded_image) |image| {
            image.copyWebLink();

            const app = @ptrCast(*c.GApplication, image.app.adw_app);
            const notification = c.g_notification_new(c.gettext(loc.notification_copied_link));
            c.g_application_send_notification(app, "copy", notification);
        }
    }

    fn onCopyImageClicked(button: *c.GtkButton, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        if (self.details_view.loaded_image) |image| {
            image.copyImage();

            const app = @ptrCast(*c.GApplication, image.app.adw_app);
            const notification = c.g_notification_new(c.gettext(loc.notification_copied_image));
            c.g_application_send_notification(app, "copy", notification);
        }
    }

    fn onErrorBarClicked(bar: *c.GtkInfoBar, user_data: c.gpointer) callconv(.C) void {
        c.gtk_info_bar_set_revealed(bar, @boolToInt(false));
    }

    fn onPreferencesClicked(action: ?*c.GSimpleAction, param: ?*c.GVariant, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        self.preferences_window = PreferencesWindow.init(self);
    }

    fn onAboutClicked(action: ?*c.GSimpleAction, param: ?*c.GVariant, user_data: c.gpointer) callconv(.C) void {
        var self = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        std.log.info("About clicked", .{});

        const about = c.gtk_about_dialog_new();

        c.gtk_about_dialog_set_program_name(@ptrCast(*c.GtkAboutDialog, about), c.gettext(loc.app_name));
        c.gtk_about_dialog_set_logo_icon_name(@ptrCast(*c.GtkAboutDialog, about), config.app_id);

        const authors = [_:null](?[*:0]u8){"Alastair Toft"};
        c.gtk_about_dialog_set_authors(@ptrCast(*c.GtkAboutDialog, about), @intToPtr([*c][*c]u8, @ptrToInt(&authors)));

        c.gtk_about_dialog_set_translator_credits(@ptrCast(*c.GtkAboutDialog, about), c.gettext(loc.translator_credits));

        c.gtk_about_dialog_set_comments(@ptrCast(*c.GtkAboutDialog, about), c.gettext(loc.app_about_description));
        c.gtk_about_dialog_set_license_type(@ptrCast(*c.GtkAboutDialog, about), c.GtkLicense.GTK_LICENSE_GPL_3_0);
        c.gtk_about_dialog_set_copyright(@ptrCast(*c.GtkAboutDialog, about), "© 2021 Alastair Toft");
        c.gtk_about_dialog_set_version(@ptrCast(*c.GtkAboutDialog, about), "0.1");

        c.gtk_window_set_transient_for(@ptrCast(*c.GtkWindow, about), self.gtk_window);
        c.gtk_window_set_modal(@ptrCast(*c.GtkWindow, about), @boolToInt(true));
        c.gtk_window_present(@ptrCast(*c.GtkWindow, about));
    }

    fn onWindowDestroyed(widget: *c.GtkWidget, user_data: c.gpointer) void {
        var app = @ptrCast(*App, @alignCast(@alignOf(*App), user_data));
        app.onWindowDestroyed();
    }

    fn onDragBegin(source: *c.GtkDragSource, drag: *c.GdkDrag, widget: *c.GtkWidget) void {
        std.log.info("Drag started", .{});
    }
};
