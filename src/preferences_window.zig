const c = @import("c.zig");
const gtk = @import("gtk.zig");
const loc = @import("localized.zig");
const std = @import("std");
const App = @import("app.zig").App;
const MainWindow = @import("main_window.zig").MainWindow;
const Settings = @import("settings.zig").Settings;

pub const PreferencesWindow = struct {
    parent: *MainWindow,

    pub fn init(parent: *MainWindow) PreferencesWindow {
        const preferred_format_combo = c.adw_combo_row_new();
        c.adw_preferences_row_set_title(@ptrCast(*c.AdwPreferencesRow, preferred_format_combo), c.gettext(loc.preferences_preferred_format));

        const enum_model = c.adw_enum_list_model_new(parent.app.type_image_format);
        c.adw_combo_row_set_model(@ptrCast(*c.AdwComboRow, preferred_format_combo), @ptrCast(*c.GListModel, enum_model));

        const name_expression = c.gtk_property_expression_new(c.adw_enum_list_item_get_type(), null, "nick");
        c.adw_combo_row_set_expression(@ptrCast(*c.AdwComboRow, preferred_format_combo), name_expression);

        const image_group = c.adw_preferences_group_new();
        c.adw_preferences_group_set_title(@ptrCast(*c.AdwPreferencesGroup, image_group), c.gettext(loc.preferences_group_image));
        c.adw_preferences_group_add(@ptrCast(*c.AdwPreferencesGroup, image_group), preferred_format_combo);

        const prefs_page = c.adw_preferences_page_new();
        c.adw_preferences_page_add(@ptrCast(*c.AdwPreferencesPage, prefs_page), @ptrCast(*c.AdwPreferencesGroup, image_group));

        const prefs_window = c.adw_preferences_window_new();
        c.adw_preferences_window_add(@ptrCast(*c.AdwPreferencesWindow, prefs_window), @ptrCast(*c.AdwPreferencesPage, prefs_page));
        c.adw_preferences_window_set_search_enabled(@ptrCast(*c.AdwPreferencesWindow, prefs_window), @boolToInt(false));

        const value = @enumToInt(parent.app.settings.getPreferredImageFormat());
        c.adw_combo_row_set_selected(@ptrCast(*c.AdwComboRow, preferred_format_combo), @intCast(c_uint, value));
        _ = gtk.g_signal_connect(preferred_format_combo, "notify::selected", gtk.G_CALLBACK(onPreferredImageChanged), @ptrCast(c.gpointer, parent));

        c.gtk_window_set_transient_for(@ptrCast(*c.GtkWindow, prefs_window), parent.gtk_window);
        c.gtk_window_set_modal(@ptrCast(*c.GtkWindow, prefs_window), @boolToInt(true));
        c.gtk_window_present(@ptrCast(*c.GtkWindow, prefs_window));

        return PreferencesWindow{ .parent = parent };
    }

    fn onPreferredImageChanged(combo: *c.AdwComboRow, paramspec: *c.GParamSpec, user_data: c.gpointer) callconv(.C) void {
        var parent = @ptrCast(*MainWindow, @alignCast(@alignOf(*MainWindow), user_data));

        const value = c.adw_combo_row_get_selected(combo);

        parent.app.settings.setPreferredImageFormatFromInt(value);
    }
};
