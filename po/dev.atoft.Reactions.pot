# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the dev.atoft.Reactions package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: dev.atoft.Reactions\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-30 16:21+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Reactions"
msgstr ""

msgid "Simple GIF search."
msgstr ""

msgid "Preferences"
msgstr ""

msgid "About Reactions"
msgstr ""

msgid "Search for an image..."
msgstr ""

msgid "Load More"
msgstr ""

msgid "Welcome to Reactions!"
msgstr ""

msgid "Type to search for an image."
msgstr ""

msgid "Drag and drop to use this image."
msgstr ""

msgid "Get Link"
msgstr ""

msgid "Copy To Clipboard"
msgstr ""

msgid "Web link copied to clipboard."
msgstr ""

msgid "Image copied to clipboard."
msgstr ""

msgid "Image Settings"
msgstr ""

msgid "Preferred Image Format"
msgstr ""

#. TRANSLATORS: Two-character language code for the current language, used for the GIPHY API.
#. See https://developers.giphy.com/docs/optional-settings/#language-support
msgid "en"
msgstr ""

msgid "translator-credits"
msgstr ""



